# LEARNX - The Innovator Sandbox
LEARNX is a framework for entrepreneurs of all types - startups, ideas, internal corporate, etc., etc. - to manage and measure progress of projects from idea to execution.

LEARNX is based on the learnings and implementation of Lean Canvas by Ash Maurya (there are different forms of Lean Canvas, but I like the adaptation by Ash), Lean Startup by Eric Ries, and Agile Scrum development by Jeff Sutherland and Mike Cohn (again there are many great Scrum professionals out there, but I especially like the ideas and thoughts from these two minds).

Deployed to Heroku-18.
[ ![Codeship Status for learnxdev/innovatus-prime](https://codeship.com/projects/a4b2c090-6c3b-0134-eaaf-0a4cc10cdf6c/status?branch=master)](https://codeship.com/projects/177145)
# LEARNX is Built on the
# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
