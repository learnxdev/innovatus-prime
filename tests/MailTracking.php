<?php

use PHPUnit_Framework_Assert as PHPUnit;

trait MailTracking
{
    protected $emails = [];

    /** @before */
    public function setUpMailTracking()
    {
    /*
        Adapted from Jeffrey Way
        https://laracasts.com/series/phpunit-testing-in-laravel/episodes/12

        To be used in Behat test suite ONLY instead of phpunit
        In Behat FeatureContext __construct then call $this->setUpMailTracking()
    */
        Mail::getSwiftMailer()
            ->registerPlugin(new TestingMailEventListener($this));    
    }

    /**
     * Verifies an e-mail was sent
     */
    protected function seeEmailWasSent()
    {
        PHPUnit::assertNotEmpty($this->emails, "No emails have been sent.");
    }

    /**
     * Verifies an e-mail was not sent
     */
    protected function seeEmailWasNotSent()
    {
        PHPUnit::assertEmpty($this->emails, "Did not expect an email to be sent.");
    }
    /**
     * Verifies the number of e-mails that have been sent
     */
    protected function seeEmailsSent($count)
    {
        $emailsSent = count($this->emails);
        PHPUnit::assertCount(
            $count, $this->emails, 
            "Expected $count email(s), but $emailsSent email(s) have been sent."
             );
        // Note use of double quotes so vars can be used in message
        // return the object for continued chain test calls
        return $this;
    }

    /**
     * Verifies the e-mail sent to address
     */
    protected function seeEmailSentTo($recipient, Swift_Message $email=null)
    {
        $emailSent = $this->getEmail($email);

        PHPUnit::assertArrayHasKey(
            $recipient, $emailSent->getTo(),
            "Email was not sent to $recipient."
             );
    }

    /**
     * Verifies the e-mail sent from address
     */
    protected function seeEmailSentFrom($sender, Swift_Message $email=null)
    {
        $emailSent = $this->getEmail($email);

        PHPUnit::assertArrayHasKey(
            $sender, $emailSent->getFrom(),
            "Email was not sent from $sender."
             );
    }

    /**
     * Verifies the e-mail subject
     */
    protected function seeEmailSubject($subject, Swift_Message $email=null)
    {
        $emailSent = $this->getEmail($email);

        PHPUnit::assertContains($subject, $emailSent->getSubject());
    }

    /**
     * Verifies the e-mail body
     */
    protected function seeEmailBody($body, Swift_Message $email=null)
    {
        $emailSent = $this->getEmail($email);

        PHPUnit::assertContains($body, $emailSent->getBody());
    }
    public function addEmail(Swift_Message $email)
    {
        $this->emails[] = $email;
    }

    public function getEmail(Swift_Message $email=null)
    {
        $this->seeEmailWasSent();

        return $email ?: $this->lastEmail();
    }

    public function lastEmail()
    {
        return end($this->emails);
    }

}

/**
* Testing Mail Event Listener
*/
class TestingMailEventListener implements Swift_Events_EventListener
{
    protected $test;

    public function __construct($test)
    {
        $this->test = $test;
    }

    public function beforeSendPerformed($event)
    {
        $this->test->addEmail($event->getMessage());
    }

}