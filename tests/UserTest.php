<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;
    /**
     * Test the registration.
     *
     * @return void
     */
    public function testRegistration()
    {
        $this->visit('/register');
        $this->type('JohnDoe', 'name');
        $this->type('johndoe@email.com', 'email');
        $this->type('password', 'password');
        $this->type('password', 'password_confirmation');
        $this->press('Register');
        $this->seePageIs('/activate');
        $this->see('Activation');
        $user = \App\User::where('email', 'johndoe@email.com')->first();
        $this->assertNotEquals('', $user->pincode);

        $this->seeInDatabase('users', ['email' => 'johndoe@email.com',
            'active' => 'N',
            ]);
    }
    /**
     * Test the login of an inactive user account.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'active' => 'N',
            ]);

        $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->visit('/home')
            ->seePageIs('/activate');
    }
    /**
     * Activate a user account.
     *
     * @return void
     */
    public function testActivateUserAccount()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'N',
            ]);

        $response = $this->actingAs($user)
            ->call('POST', 'activate', array(
            '_token' => csrf_token(),
            'pincode' => 'pincode1234'
        ));
        $this->seeInDatabase('users', 
            [
                'email' => 'joe@macdoe.com',
                'active' => 'Y',
            ]);
        
        $this->assertRedirectedTo('/home');
        $this->visit('/home');
        $this->see('Idea');
    }
    /**
     * Error activating a user account with invalid pin.
     *
     * @return void
     */
    public function testErrorActivateUserAccount()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'N',
            ]);

        $response = $this->actingAs($user)
            ->call('POST', 'activate', array(
            '_token' => csrf_token(),
            'pincode' => 'pincode1235'
        ));

        $this->assertRedirectedTo('/activate', ['flash_message']);
    }

    /**
     * Test the login with an activated user account.
     *
     * @return void
     */
    public function testActivatedLogin()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'active' => 'Y',
            ]);

        $this->actingAs($user)
            ->withSession(['foo' => 'bar'])
            ->visit('/home')
            ->see('Dashboard');
    }    
}
