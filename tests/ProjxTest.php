<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjxTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    public function testAddNewProject()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'Y',
            ]);

        // Create project with title
        $response = $this->actingAs($user)
            ->json('POST', '/project', ['title' => 'Number One']);
        // Redirect
        $this->assertResponseStatus(302);
        $this->seeInDatabase('projects', [
            'title' => 'Number One',
            'user_id' => $user->id
            ]);
    }

    public function testAddNewIdea()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'Y',
            ]);

        $this->actingAs($user)
            ->json('POST', '/project', ['title' => 'Number One']);
        $this->seeInDatabase('projects', ['title' => 'Number One']);
        $project = \App\Project::where('title', 'Number One')->first();

        // Create project with title
        $post_url = '/project/'.$project->id.'/ideas';

        $response = $this->actingAs($user)
            ->json('POST', $post_url, [
                'title' => 'My first one',
                'project_id' => $project->id
                ])->seeJsonStructure(['successFlag']);

        $this->seeInDatabase('ideas', ['title' => 'My first one']);
    }

    public function testUpdateIdeaPublicGroup()
    {
        $idea = factory(App\Idea::class)->create();
        $project = App\Project::find($idea->project_id);
        $user = App\User::find($project->user_id);

        // Update project title and public flag
        $post_url = '/project/'.$project->id.'/ideas/'.$idea->id.'?_method=PUT';

        $response = $this->actingAs($user)
            ->json('POST', $post_url, [
                'title' => 'Updated title and published',
                'public' => 1
                ]);

        $this->seeInDatabase('ideas', [
            'title' => 'Updated title and published',
            'public' => 1
            ]);
    }

    public function testToggleIdeaPublicFlag()
    {
        $idea = factory(App\Idea::class)->create();
        $updatedFlag = ($idea->public) ? 0 : 1;
        $project = App\Project::find($idea->project_id);
        $user = App\User::find($project->user_id);

        $post_url = '/idea/togglepublic/'.$idea->id;

        $response = $this->actingAs($user)
            ->json('POST', $post_url, []);

        $this->assertEquals($idea->id, $updatedFlag);
    }

    public function testAddNewProduct()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'Y',
            ]);

        // Create product with title
        $this->actingAs($user)
            ->json('POST', '/product', ['title' => 'Product One']);

        $this->seeInDatabase('products', ['title' => 'Product One']);
    }

    public function testValidateAddNewProject()
    {
        $user = factory(App\User::class)->create([
            'email' => 'joe@macdoe.com',
            'password' => 'password',
            'pincode' => 'pincode1234',
            'active' => 'Y',
            ]);

        // Validate title is required
        $this->actingAs($user)
            ->json('POST', '/project', [])
            ->seeJsonStructure(['title']);

        // Unprocessable Entity
        $this->assertResponseStatus(422);
    }

}
