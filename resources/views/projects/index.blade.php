@extends('layouts.app_materialize_black')

@section('content')
<div id="home" class="container">
  @foreach ($projects as $project)
  <div class="section">
    <div class="row">
      <div class="col s12 m8 offset-m2">
          <div class="card">
            <div class="card-content">
              <a href="{{ route('idea.read', ['project' => $project->id]) }}">
                <span class="card-title">{{ $project->title }}</span>
              </a>
              <p>
              </p>
            </div>
            <div class="card-action">
              <a href="#" onClick="$('#js-edit-project').toggleClass('hide')" class="button">Options 
              </a>
              <div id="js-edit-project" class="row hide">
                <div class="card-content">
                  <p>
                    You can edit the title and share the project with your groups or everyone here.
                  </p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection
