@extends('layouts.app_materialize_black')

@section('content')
<div id="home" class="container">
  @if ($project->id)
  <div class="section">
    <div class="row">
      <div class="col s12 m8 offset-m2">
          <div class="card">
            <div class="card-content">
              <span class="card-title">{{ $project->title }}</span>
              <p>
               
              </p>
            </div>
            <div class="card-action">
              <a href="#" onClick="$('#js-edit-project').toggleClass('hide')" class="button">Options 
              </a>
              <a href="#" onClick="$('#js-add-ideas').toggleClass('hide')" class="button">New Idea 
              </a>
              <div id="js-edit-project" class="row hide">
                <div class="card-content">
                  <p>
                    You can edit the title and share the project with your groups or everyone here.
                  </p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div id="js-add-ideas" class="row hide">
      <div class="col s12 m8 offset-m2">
        <div class="card">
          <div class="card-content">
            <p>
              Michael Buckley is quoted as saying, "The only bad ideas are the ones never tried".  Well, we don't know if that is true, but we want you to put in all of your ideas anyway.  We will help you decide if and how to do them and then you can decide yourself if they were good or bad.
            </p>
            <form id="idea-add" data-zid="{{ $project->id }}"
              method="POST" action="{{ route('api.idea.create', ['project_id' => $project->id]) }}" 
              accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                  <div class="input-field col s12 m4 hide">
                    <input name="project_id" type="text" value="{{ $project->id }}">
                    <label for="project_id">Hidden Project ID</label>
                  </div>
                  <div class="input-field col s12">
                    <input placeholder="Enter a short description" 
                    length="140" name="title" type="text" class="validate">
                    <label for="title">New Idea</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                      <button type="submit" class="waves-effect waves-light btn light-green accent-4">
                          Add
                      </button>
                  </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  <div class="divider"></div>
  </div>
  @endif
  @include('ideas.index')
</div>
@endsection
