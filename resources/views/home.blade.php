@extends('layouts.app_materialize_black')

@section('content')
<div id="home" class="container">
<div class="section">
  <div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card">
          <div class="card-content">
            <h3 class="grey-text text-darken-3">LEARNX</h3>
            <p>
              Idea Execution Framework
            </p>
          </div>
          <div class="card-action">
            <a href="#" onClick="$('#js-start-idea').toggleClass('hide')" class="button">Add New 
            </a>
            <a href="#" onClick="goToIdeas()">Ideas </a>
            <div id="js-start-idea" class="row hide">
              <div class="card-content">
                <p>
                  Start by entering a title for your LEARNX Project.</p>
                <p>
                  A LEARNX Project is a collection of project artefacts.  Every project is different so that means the project artefacts will also be different for each project.  A project can be multiple projects like a program or small like a sub-project or experiment.<br>
                  Use the LEARNX Project to help you collect and group artefacts for your project.
                </p>
                <p>
                  To start with enter a simple title or name that describes your next big idea.  Use "Number One" if you can't think of anything.  You can always change it later so don't dwell on this too long.  Let's just get started!
                </p>
              </div>
              <div class="card-content">
              <form id="projx-add"
                method="POST" action="{{ url('project') }}" 
                accept-charset="UTF-8" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="input-field col s12 m4">
                      <input placeholder="Enter a short title" length="20" name="title" type="text" class="validate">
                      <label for="title">Project Title</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col s12">
                        <button type="submit" class="waves-effect waves-light btn light-green accent-4">
                            Add
                        </button>
                    </div>
                  </div>
              </form>
              </div>
            </div>
          </div>

        </div>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">L</span>
            <p>
            <a class="dropdown-button" href="#!" onClick="$('#l_more').slideToggle()">
            Look & Listen
            <i class="material-icons right">keyboard_arrow_down</i>
            </a>
            </p>
            <p id="l_more" style="display:none">
            Start and end with "The Why?". Describe your idea and create a
            clear mission statement for yourself and your team.  <br>
            This section helps you clarify and define your project charter, Mission Statement
            Project Charter, Mission Statement,  <br><a class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="A Problem Statement describes what pain point you are relieving for your customer.">Problem Statement</a>
            Problem Statement, Statement of Work, <br>
            Stakeholder Identification
            </p>
          </div>
          <div class="card-action">
            <a href="#">What & Why </a>
          </div>
        </div>
    </div>
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">E</span>
            <p>
            <a class="dropdown-button" href="#!" onClick="$('#e_more').slideToggle()">
            Evaluate & Elaborate 
            <i class="material-icons right">keyboard_arrow_down</i>
            </a>
            </p>
            <p id="e_more" style="display:none">
            The What  Lean Canvas  -Solution Statement,  <br>
            Project Plan (Hi Level) - Themes, Release Burn Down, 
            Resource Allocation & Team Identification
            </p>
          </div>
          <div class="card-action">
            <a href="#">What, Who & When </a>
          </div>
        </div>
    </div>
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">A</span>
            <p>
              <a class="dropdown-button" href="#!" onClick="$('#a_more').slideToggle()">
              Adapt & Agree  
              <i class="material-icons right">keyboard_arrow_down</i>
              </a>
            </p>
            <p id="a_more" style="display:none">
            Create an Experiment Loop Map<br> 
            Detailed Plan including Feature Epics, User stories,
            Sprint Plan, Sprint Burn Down, Work Packages, Req Specs
            </p>
          </div>
          <div class="card-action">
            <a href="#">What & How </a>
          </div>
        </div>
    </div>
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">R</span>
            <p>
              <a class="dropdown-button" href="#!" onClick="$('#r_more').slideToggle()">
              Run & Register 
              <i class="material-icons right">keyboard_arrow_down</i>
              </a>
            </p>
            <p id="r_more" style="display:none">
            <a href="#">Learning Matrix </a><br>
            Metrics and Risk Register, Next Stage Plan  <br>
            Work package status reports, Sprint Reports
            </p>
          </div>
          <div class="card-action">
            <a href="#">Learnings </a>
          </div>
        </div>
    </div>
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">N</span>
            <p>
              <a class="dropdown-button" href="#!" onClick="$('#n_more').slideToggle()">
              Next or Not? 
              <i class="material-icons right">keyboard_arrow_down</i>
              </a>
            </p>
            <p id="n_more" style="display:none">
            Decision Workshops and Retropectives   <br>
            End Stage Report, Exception Plan, Highlights and Exception Report
            </p>
          </div>
          <div class="card-action">
            <a href="#">Decisions </a>
          </div>
        </div>
    </div>
    <div class="col s12 m6 l4">
        <div class="card">
          <div class="card-content">
            <span class="card-title">X</span>
            <p>
              <a class="dropdown-button" href="#!" onClick="$('#x_more').slideToggle()">
              Experiment 
              <i class="material-icons right">keyboard_arrow_down</i>
              </a>
            </p>
            <p id="x_more" style="display:none">
            <a href="#">LEARN ProjX: Lean and Agile Experiment Projects </a>
            ProjX consist of all the artefacts in the framework that help you
            build your <a class="tooltipped" data-position="bottom" 
            data-delay="50" 
            data-tooltip="Most Valuable Experiments and Minimum Viable Experiments.
            Experiments that show the most relevant value for your idea executed
            at the most effective way to prove your idea.">MVX</a>. 
             <br> <br>
            </p>
          </div>
          <div class="card-action">
            <a href="{{ url('/projects') }}" title="View Projects">
            View Projx 
            </a>
          </div>
        </div>
    </div>
  </div>
<div class="divider"></div>
<script type="text/javascript">
  function goToIdeas() {
    $('#home').toggleClass('hide');
    $('#dashboard').toggleClass('hide');
  }
</script>
</div>

<div id="dashboard" class="container hide">
<h4 class="white-text">My Ideas</h4>
  <div class="row">
    <div class="col s12">
  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Project Number One</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="/project/1/ideas" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle green">insert_chart</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle red">play_arrow</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  </ul>
    </div>

  </div>
<div class="divider"></div>
  <h4 class="header white-text">New Ideas</h4>
  <div class="row">
    <div class="col s12">
  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle green">insert_chart</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle red">play_arrow</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  </ul>
    </div>

  </div>
  </div>

</div>

@endsection
