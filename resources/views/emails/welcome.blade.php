<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" vbz-center="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LEARNX</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway';
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .vbz-center {
                text-align: center;
            }
        </style>
    </head>
    <body>
      <div>
        <h3 class="vbz-center">Welcome to Innovator Sandbox</h3>
        <p>Hi {{ $user->name }}.  My name is Kevin and I am the creator of the LEARNX framework.</p>
        <p>Thanks for joining our community of learners, builders, dreamers, and doers.</p>
        <p>To activate your account and get started use the following pincode: </p>
        <p class="vbz-center">{{ $user->pincode }}</p>
      </div>
    </body>
</html>
