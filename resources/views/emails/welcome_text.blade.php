Welcome to the Innovator Sandbox!

Hi {{ $user->name }}.  My name is Kevin and I am the creator of the LEARNX framework.
Thanks for joining our community of learners, builders, dreamers, and doers.

To activate your account and get started use the following pincode: 

    {{ $user->pincode }}