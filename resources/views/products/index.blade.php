@extends('layouts.app_materialize_black')

@section('content')
<div id="home" class="container">
<div class="section">
  <div class="row">
    <div class="col s12 m8 offset-m2">
        <div class="card">
          <div class="card-content">
            <h3 class="grey-text text-darken-3">LEARNX</h3>
            <p>
              Idea Execution Framework
            </p>
          </div>
          <div class="card-action">
            <a href="#" onClick="$('#js-start-idea').toggleClass('hide')" class="button">Start New 
            </a>
          <div id="js-start-idea" class="row hide">
          <div class="card-content">
            <p>
              Start by entering a title for your LEARNX product.</p>
            <p>
              A simplified definition of product is an end result. Our definition extends this to a collection, or grouping, of the artefacts that lead to an end result.  Artefacts differ for everyone and for every project, so we use the product to help you group artefacts like ideas, notes, and plans.</p>
            <p>
              To start with enter a simple title or name that describes your next big idea.  Use "Number One" if you can't think of anything.  You can always change it later so don't dwell on this too long.  Let's just get started!
            </p>
          </div>
          <div class="card-content">
            <form id="product-add"
              method="POST" action="{{ route('idea.create', ['id' => $idea->id]) }}" 
              accept-charset="UTF-8" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                  <div class="input-field col s12 m4 hide">
                    <input name="project_id" type="text" value="{{ $idea->id }}">
                    <label for="project_id">Hidden Project ID</label>
                  </div>
                  <div class="input-field col s12 m4">
                    <input placeholder="Enter a short title" length="20" name="title" type="text" class="validate">
                    <label for="title">Idea Title</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col s12">
                      <button type="submit" class="waves-effect waves-light btn light-green accent-4">
                          Add
                      </button>
                  </div>
                </div>
            </form>
          </div>
          </div>

        </div>
    </div>
  </div>
  </div>
  <div class="row">
  </div>
<div class="divider"></div>
</div>

<div id="dashboard" class="container">
<h4 class="white-text">My Ideas</h4>
  <div class="row">
    <div class="col s12">
  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle green">insert_chart</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle red">play_arrow</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  </ul>
    </div>

  </div>
<div class="divider"></div>
  <h4 class="header white-text">New Ideas</h4>
  <div class="row">
    <div class="col s12">
  <ul class="collection">
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle">folder</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle green">insert_chart</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    <li class="collection-item avatar">
      <i class="material-icons circle red">play_arrow</i>
      <span class="title">Title</span>
      <p>First Line <br>
         Second Line
      </p>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  </ul>
    </div>

  </div>
  </div>

</div>

@endsection
