<div id="ideas-index" class="section">
  <h4 class="white-text">{{ $ideasIndexTitle or '' }}</h4>
  <div class="row">
    <div class="col s12">
      <ul class="collection">
      @foreach ($ideas as $idea)
        <li class="collection-item avatar">
          <i class="material-icons circle">folder</i>
          <span class="title">{{ $idea->title }}</span>
          <p>
            What? <br>
            Why is this important for you? <br>
            Why is this important for your customer?
          </p>
            @can('update', $project)
            <div class="switch">
              <label>
                Make Public
                <input type="checkbox" data-zid="{{ $idea->id }}" {{ ($idea->public) ? 'checked': '' }}>
                <span class="lever"></span>
              </label>
            </div>
            @endcan
          <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
        </li>
      @endforeach
      </ul>
    </div>
  </div>
<div class="divider"></div>
</div>

