<script src="/js/app.js"></script>
<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/color/jquery.color.plus-names-2.1.2.min.js" integrity="sha256-Wp3wC/dKYQ/dCOUD7VUXXp4neLI5t0uUEF1pg0dFnAE=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
<script type="text/javascript">
  $( document ).ready(function(){
    $(".button-collapse").sideNav();
  });
  $('.js-logout').click(function() {
    $('#modal_leave').openModal();
      });

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script type="text/javascript">
  $(function() {
    $('#project-add').on('submit', function(e) {
      e.preventDefault(e);
      var urlToCall = '/project';
      form_data = new FormData( this );

      $.ajax({
        url: urlToCall,
        type: 'post',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          alert('Yay!');

        },
        error: function(xhr, textStatus) {
          alert('Boooo!');
          console.log(xhr.status, textStatus);
        }

      });
    });
  });

  $(function() {
    $('#idea-add').on('submit', function(e) {
      e.preventDefault(e);
      var urlToCall = '/project/' + $(this).data('zid') + '/ideas';
      form_data = new FormData( this );

      $.ajax({
        url: urlToCall,
        type: 'post',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          document.location.reload(true);
        },
        error: function(xhr, textStatus) {
          var responseErrors = JSON.parse(xhr.responseText);
          if (responseErrors.title) {
            Materialize.toast(responseErrors.title, 4000);
          } else {
            // Materialize.toast(message, displayLength, className, completeCallback);
            Materialize.toast('Sorry an error occurred. Please try again.', 4000);           
          }
          console.log(xhr.status, textStatus, xhr.responseText['title'], xhr.responseText);
        }

      });
    });
  });

  $(function() {
    $('#product-add').on('submit', function(e) {
      e.preventDefault(e);
      var urlToCall = '/product';
      form_data = new FormData( this );

      $.ajax({
        url: urlToCall,
        type: 'post',
        data: form_data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
          alert('Yay!');

        },
        error: function(xhr, textStatus) {
          alert('Boooo!');
          console.log(xhr.status, textStatus);
        }

      });
    });
  });
</script>

<script type="text/javascript">
  $(':checkbox').change(function() {
        if($(this).is(":checked")) {
          //'checked' event code
        } else {
          //'unchecked' event code
        }

    var zid = $(this).data('zid');
    var urlToCall = '/idea/togglepublic/'+zid;

    form_data = {
      id: zid
    };

    $.ajax({
      url: urlToCall,
      type: 'post',
      data: form_data,
      contentType: false,
      processData: false,
      dataType: 'json',
      success: function(data) {
        if (data.successFlag) {
          return;
        } else {
          Materialize.toast('Sorry an error occurred. Is this your idea?', 4000);
        }
      },
      error: function(xhr, textStatus) {
        console.log(xhr.status, textStatus, xhr.responseText);
      }
    });
  });
</script>