  <div id="navispinner" class="preloader-wrapper hide"
    style="position: fixed; margin-top: 80px; 
    margin-left: auto; margin-right: auto; 
    left: 0; right: 0; z-index:10">
    <div class="spinner-layer spinner-yellow-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>
  <div id="modal_leave" class="modal">
    <div class="modal-content">
      <h4>Leaving so soon?</h4>
      <p>Time to get started doing, but come back soon to update your progress!</p>
    </div>
    <div class="modal-footer">
      <a href="{{ url('/logout') }}" 
        class="modal-action modal-close waves-effect waves-green btn-flat"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          Log Out
      </a>
    </div>
  </div>    
@if (session('flash_message'))
<!-- Flash Messages -->
    <div class="row">
      <div id="flashmsg" class="col s12 m6 offset-m3">
        <div class="card-panel teal">
          <span class="white-text">{{ session('flash_message') }}
          </span>
          <a class="white-text right" href="#" onClick="$('#flashmsg').addClass('hide')">x</a>
        </div>
      </div>     
    </div>
@endif
@if (count($errors) > 0)
    <div class="row">
      <div class="col s12 m6 offset-m3 card-panel red darken-1 white-text text-darken-1">
          <h5>Whoops!</h5>
          <p>Looks like there were some problems with what you are trying to do.</p>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
    </div>
@endif