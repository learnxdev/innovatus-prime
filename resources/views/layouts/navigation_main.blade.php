<div class="navbar-fixed">
  <nav class="black" role="navigation">
    <div class="nav-wrapper container">
    @if (Auth::guest())
      <a href="/" class="brand-logo hide-on-med-and-down">InnovatorSandbox</a>
      <ul id="slide-out" class="side-nav">
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
      </ul>
      <ul class="right hide-on-med-and-down">
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
      </ul>
    @else
      <a href="/home" class="brand-logo hide-on-med-and-down">InnovatorSandbox</a>
      <ul class="side-nav" id="slide-out">
        <li>
          <a href="{{ url('profile') }}" title="Profile">
            {{ Auth::user()->name }}
          </a>
        </li>
        @if (request()->is('home'))
        <li class="active">
        @else
        <li>
        @endif
          <a href="{{ url('home') }}" title="Home">
              Home
          </a>
        </li>
        @if (request()->is('ideas'))
        <li class="active">
        @else
        <li>
        @endif
          <a href="{{ url('ideas') }}" title="Ideas">
              Ideas
          </a>
        </li>
        <li>
          <a href="#" title="Log Out" class="js-logout">
            Log Out
          </a>
        </li>
      </ul>      
      <ul class="right hide-on-med-and-down">
        @if (request()->is('home'))
        <li class="active">
        @else
        <li>
        @endif
          <a href="{{ url('home') }}" title="Home">
          <i class="material-icons">home</i></a>
        </li>
          <li>
            <a class="dropdown-button" href="#!" data-activates="dropdown_menu">
              {{ Auth::user()->name }}
              <i class="material-icons right">arrow_drop_down</i>
            </a>
          </li>
          <ul id='dropdown_menu' class='dropdown-content'>
          @if (request()->is('profile'))
          <li class="active">
          @else
          <li>
          @endif
            <a href="{{ url('profile') }}" title="Profile">
            <i class="material-icons">settings</i></a>
          </li>
          @if (request()->is('idea'))
          <li class="active">
          @else
          <li>
          @endif
            <a href="{{ url('ideas') }}" title="Ideas">
            <i class="material-icons">lightbulb_outline</i></a>
          </li>
          <li>
            <a href="#" title="Log Out" class="js-logout">
              Log Out
            </a>
          </li>
        </ul>
      </ul>
      @endif
      <a href="#" data-activates="slide-out" class="button-collapse">
        <i class="material-icons">menu</i>
      </a>
      <form id="logout-form" action="{{ url('/logout', array(), true) }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
    </div>
  </nav>
</div>