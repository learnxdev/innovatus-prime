@extends('layouts.app_materialize')

@section('content')
<div class="container">
  <div class="section">
    <div class="row">
        <div class="col s12 m8 offset-m2">
          <div class="card">
            <div class="card-content">
                <span class="card-title">Activation</span>
                <p>
                    Please check your email for an activation code.
                </p>
            </div>
          </div>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="card">
      <div class="col s12 m8 offset-m2">
        <form 
          method="POST" action="{{ url('/activate') }}" 
          accept-charset="UTF-8" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
              <div class="input-field col s6">
                <input placeholder="Enter pincode" name="pincode" type="text" class="validate">
                <label for="pincode">Activation Pincode</label>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                <button type="submit" class="waves-effect waves-light btn">
                    Activate
                </button>
                <a class="waves-effect waves-teal btn-flat" href="{{ url('/activate/send/resend') }}">
                    Send new activation pincode
                </a>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection