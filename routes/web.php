<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Controller constructor middleware call
Route::get('/home', 'HomeController@index');
Route::get('/activate', 'HomeController@getActivate');
Route::post('/activate', 'HomeController@postActivate');
Route::get('/activate/send/{resend?}', 'HomeController@mailActivate');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', function () {
        return "Hello User!";
    });
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/project/{project}/ideas', 'IdeaController', 
    [
        'only' => [
        'index', 'store', 'update', 'destroy'
        ], 
        'names' => [
        'store' => 'idea.create', 
        'index' => 'idea.read', 
        'update' => 'idea.update', 
        'destroy' => 'idea.delete'
        ]
    ]);

    Route::get('/ideas', 'IdeaController@index');
    Route::post('/idea/togglepublic/{idea}', 'IdeaController@updatePublicFlag');

    Route::get('/product', 'ProductController@index');
    Route::post('/product', 'ProductController@store');

    Route::get('/projects', 'ProjectController@index');
    Route::post('/project', 'ProjectController@store');
    Route::get('/project/{project}', 'ProjectController@view');

});
