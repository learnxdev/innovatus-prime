<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/v1/idea', 'Api\IdeaController', 
[
    'only' => [
    'index', 'store', 'update', 'destroy'
    ], 
    'names' => [
    'store' => 'api.idea.create', 
    'index' => 'api.idea.read', 
    'update' => 'api.idea.update', 
    'destroy' => 'api.idea.delete'
    ]
]);

//Route::group(['middleware' => ['auth:api']], function () {
//});
