<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->realText(40, 1),
        'group_id' => 0,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
    ];
});

$factory->defineAs(App\Project::class, 'publicProject', function (Faker\Generator $faker) {
    return [
        'title' => $faker->realText(40, 1),
        'group_id' => 1,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
    ];
});

/**
 * Create a private/non-shared idea on a private project
 */
$factory->define(App\Idea::class, function (Faker\Generator $faker) {
    $user = factory(App\User::class)->create();
    $project = factory(App\Project::class)->create([
                'user_id' => $user->id,
                ]);

    return [
        'title' => $faker->realText(40, 1),
        'public' => 0,
        'user_id' => $user->id,
        'project_id' => $project->id,
    ];
});

/**
 * Create a public idea for a user on a public project
 */
$factory->defineAs(App\Idea::class, 'publicIdea', function ($faker) {
    return [
        'title' => 'A Public Idea',
        'public' => 1,
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'project_id' => function () {
            return factory(App\Project::class, 'publicProject')->create()->id;
        },
    ];
});

/**
 * Create a private/non-shared idea on a private project
 */
$factory->defineAs(App\Idea::class, 'byUserAndProject', function (Faker\Generator $faker) {
    // user and project id should be overridden in factory call
    return [
        'title' => $faker->realText(40, 1),
        'public' => 0,
        'user_id' => 0,
        'project_id' => 0,
    ];
});

