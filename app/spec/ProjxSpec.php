<?php

namespace spec\App\Acme;

use App\Acme\Projx;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ProjxSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Projx::class);
    }
}
