<?php

namespace App\Policies;

use App\User;
use App\Project;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function isOwner(User $user, Project $project)
    {
        return $user->id == $project->user_id;
    }

    public function isPublic(User $user, Project $project)
    {
        return $project->group_id === 1;
    }

    public function isOwnerOrPublic(User $user, Project $project)
    {
        return $this->isOwner($user, $project) 
            || $this->isPublic($user, $project);
    }

    public function isProjectGroupMember(User $user, Project $project)
    {
/*
        $isMember = $user->groups()->where('id', $project->group_id)->first();
        if ($isMember) {
            return true;
        }
*/        
        return false;
    }
    /**
     * Determine whether the user can view the project.
     *
     * @param  App\User  $user
     * @param  App\Project  $project
     * @return mixed
     */
    public function view(User $user, Project $project)
    {
        return ($this->isOwnerOrPublic($user, $project)) 
            ? : $this->isProjectGroupMember($user, $project);
    }

    /**
     * Determine whether the user can create projects.
     *
     * @param  App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        // Everyone can create their own projects
        return true;
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param  App\User  $user
     * @param  App\Project  $project
     * @return mixed
     */
    public function update(User $user, Project $project)
    {
        return ($this->isOwnerOrPublic($user, $project)) 
            ? : $this->isProjectGroupMember($user, $project);
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param  App\User  $user
     * @param  App\Project  $project
     * @return mixed
     */
    public function delete(User $user, Project $project)
    {
        return $this->isOwner($user, $project);
    }
}
