<?php

namespace App\Http\Controllers;

use App\Project;
use App\Http\Requests;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    protected $returnData;
    protected $errorMessage = 'An error occurred with the request.';
    protected $errorFlag = true;

    /**
     * Initialize a new Project.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->errorData = [
            'errorMessage' => $this->errorMessage,
            'errorFlag' => $this->errorFlag
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = ($this->request->user()) 
            ? Project::where('user_id', $this->request->user()->id)->get()
            : Project::where('group_id', 1)->get();

        if ($this->request->ajax()) {
            return $projects;
        } else {
            return view('projects.index', 
                compact('projects'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = $this->validate($this->request, [
            'title' => 'required|max:20',
        ]);

        if ($validator) {
            return $validator;
        }

        $project = new Project($this->request->all());
        $project->user_id = $this->request->user()->id;
        $project->save();


        if ($this->request->ajax()) {
            return response()->json($project);
        } else {
            $url = route('idea.read', ['id' => $project->id]);
            return redirect($url);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
