<?php

namespace App\Http\Controllers;

use App\Idea;
use App\Project;
use App\Http\Requests;
use Illuminate\Http\Request;

class IdeaController extends Controller
{
    protected $returnData;
    protected $successFlag = false;
    protected $errorFlag = false;
    protected $errorMessage = 'If errorFlag is true then an error occurred.';
    protected $ideasIndexTitle = 'My Next Big Idea';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->returnData = [
            'successFlag' => $this->successFlag,
            'errorFlag' => $this->errorFlag,
            'errorMessage' => $this->errorMessage,
        ];

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        $ideas = $this->filterIdeasByProject($project);

        $ideasIndexTitle = $this->ideasIndexTitle;

        if ($this->request->ajax()) {
            return $ideas;
        } else {
            return view('projects.ideas', 
                compact('project', 'ideas', 'ideasIndexTitle'));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterIdeasByProject(Project $project)
    {
        if ($project->id) {
            $ideas = Idea::where('project_id', $project->id)->get();
        } else {
            $this->ideasIndexTitle = 'Public Ideas';
            $ideas = Idea::where('public', 1)->get();
        }

        return $ideas;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project)
    {
        $validator = $this->validate($this->request, [
            'title' => 'required|max:140',
        ]);

        $idea = new Idea($this->request->all());
        $idea->user_id = $this->request->user()->id;
        $idea->project_id = $project->id;

        // populate returnData with other data keys as needed
        $this->returnData['id'] = $idea->id;
        $this->returnData['title'] = $idea->title;
        $this->returnData['successFlag'] = $idea->save();
        $this->returnData['message'] = 'Idea created.';

        return response()->json($this->returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Project  $project
     * @param  App\Idea  $idea
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Idea $idea)
    {
        // populate returnData with other data keys as needed ie. debug
        $this->returnData['successFlag'] = 
            ($this->request->user()->can('update', $project))
            ? $idea->update($this->request->all()) 
            : false;
        $this->returnData['message'] = 'Idea updated.';
        $this->returnData['id'] = $idea->id;
        $this->returnData['title'] = $idea->title;
        $this->returnData['projectUserID'] = $project->user_id;
        $this->returnData['requestUserID'] = $this->request->user()->id;

        return response()->json($this->returnData);
    }

    public function updatePublicFlag(Idea $idea)
    {
        $project = Project::find($idea->project_id);
        $public = ($idea->public) ? 0 : 1;

        $this->returnData['id'] = $idea->id;
        $this->returnData['successFlag'] = 
            ($this->request->user()->can('update', $project))
            ? $idea->update(['public' => $public]) 
            : false;
        $this->returnData['message'] = 'Public flag updated.';

        return response()->json($this->returnData);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
