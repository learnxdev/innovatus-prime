<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeToInnovatorSandbox;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->request->user()->active == 'N') {
            return redirect('/activate');
        }

        return view('home');
    }

    /**
     * Show the user account activation page.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActivate()
    {
        return view('users.activation');
    }

    /**
     * Verify pincode and user to activate the user account.
     *
     * @return \Illuminate\Http\Response
     */
    public function postActivate()
    {
        if ($this->request->input('pincode') 
            == $this->request->user()->pincode) {
            $user = $this->request->user();
            $user->active = 'Y';
            $user->save();

            return redirect('home');
        }

        return redirect('activate')
            ->with('flash_message', 'Not a valid pincode for your login credentials.');
    }

    /**
     * Mail activation pincode to the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function mailActivate($resend='')
    {
        $user = $this->request->user();
        $emailInfo = new WelcomeToInnovatorSandbox($user);

        Mail::to($user->email)->send($emailInfo);

        if ($resend == '') {
            return redirect('activate');
        } else {
            return redirect('activate')
                ->with('flash_message', 'A new e-mail has been sent to you.');
        }


    }
}
