<?php

namespace App\Http\Controllers\Api;

use App\Idea;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IdeaController extends Controller
{
    protected $returnData;
    protected $successFlag = false;
    protected $errorFlag = false;
    protected $errorMessage = 'An error occurred with the request.';

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->returnData = [
            'successFlag' => $this->successFlag,
            'errorFlag' => $this->errorFlag,
            'errorMessage' => $this->errorMessage,
        ];

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Idea::where('public', 1)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idea = new Idea($request->all());
        if ($request->input('title') && $request->input('project_id')) {
            $idea->project_id = $request->input('project_id');
            $this->returnData['successFlag'] = $idea->save();           
            $this->returnData['title'] = $request->input('title');           
            $this->returnData['project_id'] = $request->input('project_id');           
            // return response()->json($idea);
        } else {
            $this->returnData['successFlag'] = false;           
        }
        // populate returnData with other data keys as needed
        $this->returnData['message'] = 'Idea created successfully';

        return response()->json($this->returnData);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
