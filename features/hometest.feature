@codeship
Feature:  Welcome
  In order to prove the basics work as intended
  We want to test the landing page functionality

  Scenario: The one where we check the landing page
    When I am on the homepage
    Then I should see "Laravel"

  Scenario: The one where we check the registration page
    When I go to "register"
    Then the url should match "register"

  Scenario: The one where we check the log in page
    When I go to "login"
    Then the url should match "login"
    And I can begin tests with Laravel