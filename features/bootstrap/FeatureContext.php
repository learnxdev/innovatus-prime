<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use PHPUnit_Framework_Assert as PHPUnit;
use Laracasts\Behat\Context\Migrator;
use Laracasts\Behat\Context\DatabaseTransactions;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends Behat\MinkExtension\Context\MinkContext 
    implements Context, SnippetAcceptingContext
{
    use Migrator, DatabaseTransactions, MailTracking;

    protected $httpClient;
    protected $response;
    protected $base_uri = 'http://localhost:8000';

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->setUpMailTracking();
        $guzzleBaseURI = env('BEHAT_BASE_URI', $this->base_uri);
        $this->httpClient = new Client(['base_uri' => $guzzleBaseURI]);
    }
    /**
     * Flag the test database as ready for testing.
     *
     * @var bool
     */

    /**
     * @Then I can begin tests with Laravel
     */
    public function iCanBeginTestsWithLaravel()
    {
        if (env('APP_ENV') == 'dev') {
            PHPUnit::assertEquals('.env.behat', app()->environmentFile());
            PHPUnit::assertEquals('dev', env('APP_ENV'));
        } else {
            PHPUnit::assertEquals('.env.codeship', app()->environmentFile());
            PHPUnit::assertEquals('test', env('APP_ENV'));            
        }
        PHPUnit::assertTrue(config('app.debug'));
    }

    /**
     * @Given Fred is a visitor
     */
    public function fredIsAVisitor()
    {
        $this->iSignInWithInvalidCredentials();
    }

    /**
     * @Given :username has a member account :email
     */
    public function hasAMemberAccount($username, $email)
    {
        $user = factory(App\User::class)->create([
            'name' => $username,
            'email' => $email,
            'password' => bcrypt('password'),
            'type' => 'member',
            'pincode' => 'Not0OrBlank',
            'active' => 'Y',
            ]);

        $this->user_id = $user->id;
        $this->username = $username;
        $this->email = $email;

        $this->iSignIn();
    }

    /**
     * @When I register :username :email
     */
    public function iRegister($username, $email)
    {
        Auth::logout();
        $this->visit('register');

        $this->fillField('name', $username);
        $this->fillField('email', $email);
        $this->fillField('password', 'password');
        $this->fillField('password_confirmation', 'password');

        $this->pressButton('Register');

        $user = App\User::where('email', $email)->first();
        $this->user_id = $user->id;
        $this->username = $username;
        $this->email = $email;
    }

    /**
     * @Then I should have an account
     */
    public function iShouldHaveAnAccount()
    {
        $this->assertSignedIn();
    }

    /**
     * @Given I have an account :username :email
     */
    public function iHaveAnAccount($username, $email)
    {
        // Simulate checking the account by Registering a new user and then 
        // log the user out.  If both succeed then user has an account
        $this->iRegister($username, $email);
        $user = \App\User::where('email', $email)->first();
        $user->pincode = 'pincode1234';
        $user->save();
        Auth::logout();
        //PHPUnit::assertEquals(null, Auth::logout());
    }

    /**
     * @When I sign in
     */
    public function iSignIn()
    {
        Auth::logout();
        $this->visit('login');

        $this->fillField('email', $this->email);
        $this->fillField('password', 'password');

        $this->pressButton('Login');
    }

    /**
     * @When I sign in with invalid credentials
     */
    public function iSignInWithInvalidCredentials()
    {
        $this->username = "FredFlintstone";
        $this->email = "fredlintstone@email.com";

        $this->iSignIn();
    }

    /**
     * @Then I should be logged in
     */
    public function iShouldBeLoggedIn()
    {
        $this->assertSignedIn();
    }

    /**
     * @Then I should not be logged in
     */
    public function iShouldNotBeLoggedIn()
    {
        $this->assertNotSignedIn();
    }
    /**
     * Verifies user credentials are authenticated
     */
    private function assertSignedIn()
    {
        PHPUnit::assertTrue(Auth::check());
    }

    /**
     * Verifies user credentials are not authenticated
     */
    private function assertNotSignedIn()
    {
        PHPUnit::assertTrue(Auth::guest());
        //$this->printLastResponse();
        //PHPUnit::assertPageAddress('login');
    }

    /**
     * @Then :username should receive an activation email
     */
    public function shouldReceiveAnActivationEmail($username)
    {
        PHPUnit::assertEquals($this->username, $username);
/*
        Mail::raw('Account activation', function ($message) {
            $message->to($this->email);
            $message->from('coach@innovatorsandbox.com');
        });
*/
        $this->visit('activate/send');
        $this->seeEmailWasSent();
        $this->seeEmailsSent(2);    // one for html and one for text
        $this->seeEmailSentTo($this->email);
        $this->seeEmailSubject('Welcome');
        $this->seeEmailBody('pincode');
        $this->seeEmailSentFrom('coach@innovatorsandbox.com');
    }

    /**
     * @When I activate account pin :pincode
     */
    public function iActivateAccountPin($pincode)
    {
        $this->iSignIn();
        $this->visit('activate');

        $this->fillField('pincode', $pincode);

        $this->pressButton('Activate');
    }

    /**
     * @Then /^the response code should be (\d+)$/
     */
    public function theResponseCodeShouldBe($response_code)
    {
        PHPUnit::assertEquals($response_code, $this->response->getStatusCode());
    }

    /**
     * @Given /^the JSON response should have a "([^"]*)" containing "([^"]*)"$/
     */
    public function theJsonResponseShouldHaveAContaining($var_name, $var_contain_val)
    {
        $json_data = json_decode($this->response->getBody(true));

        PHPUnit::assertFalse(empty($json_data));
        PHPUnit::assertTrue(isset($json_data->$var_name));
        PHPUnit::assertEquals($var_contain_val, $json_data->$var_name);
    }

    /**
     * @When I add a new project :title
     */
    public function iAddANewProject($title)
    {
        // Validate API call and then mock actions

        // If above passed then we move on to mock actions
        $project = factory(App\Project::class)->create([
            'user_id' => $this->user_id,
            'title' => $title,
            ]);
        $this->project_id = $project->id;

        $this->visit('project/'.$this->project_id.'/ideas');
    }

    /**
     * @When I add a new idea :title
     */
    public function iAddANewIdea($title)
    {
        // Validate API call and then mock actions
        // This is a draft test to verify using Guzzle. 
        // API routes and calls still needs auth tokens implemented
        $this->response = $this->httpClient->request('POST', '/api/v1/idea', [
                    'form_params' => [
                        'project_id' => 1,
                        'title' => 'Behat API Test Project',
                    ]
                ]);
        $response_code = 200;
        $var_name = 'successFlag';
        $var_contain_val = true;
        $this->theJsonResponseShouldHaveAContaining($var_name, $var_contain_val);
        $this->theResponseCodeShouldBe($response_code);

        // If above passed then we move on to mock actions
        $this->iOwnAnIdea($title);
    }

    /**
     * @Given I own an idea :title
     */
    public function iOwnAnIdea($title)
    {
        $titleOfProject = 'Add Idea Project';
        $this->iAddANewProject($titleOfProject);
        $idea = factory(App\Idea::class)->create([
            'project_id' => $this->project_id,
            'user_id' => $this->user_id,
            'title' => $title,
            ]);

        $ownIdea = App\Idea::where('title', $title)
            ->where('user_id', $this->user_id)->get();

        $this->idea = $ownIdea;

        PHPUnit::assertEquals(1, $this->idea->count());
        $this->visit('project/'.$this->project_id.'/ideas');
    }

    /**
     * @When I share the idea with everyone
     */
    public function iShareTheIdeaWithEveryone()
    {
        // Validate API call and then mock actions
        // This is a draft test to verify using Guzzle with session auth
        // Session authentication still needs to be implemented
/*        $this->response = $this
            ->httpClient
                ->request(
                    'POST', 
                    '/idea/togglepublic/'.$this->idea[0]->id, 
                    [
                    'form_params' => [
                        'project_id' => 1,
                        'title' => 'Behat API Test Project',
                    ]
                ]);
        $response_code = 200;
        $var_name = 'successFlag';
        $var_contain_val = true;
        $this->theJsonResponseShouldHaveAContaining($var_name, $var_contain_val);
        $this->theResponseCodeShouldBe($response_code);
*/
        // If above passed then we move on to mock actions
        $updateIdea = App\Idea::find($this->idea[0]->id);
        PHPUnit::assertEquals($updateIdea->user_id, $this->user_id);
        $updateIdea->public = 1;
        $updateIdea->save();
}

    /**
     * @Then I can not share the idea
     */
    public function iCanNotShareTheIdea()
    {
        // Validate API call and then mock actions
        // This is a draft test to verify using Guzzle with session auth
        // Session authentication still needs to be implemented
/*        $this->response = $this
            ->httpClient
                ->request(
                    'POST', 
                    '/idea/togglepublic/'.$this->idea[0]->id, 
                    [
                    'form_params' => [
                        'project_id' => 1,
                        'title' => 'Behat API Test Project',
                    ]
                ]);
        $response_code = 200;
        $var_name = 'successFlag';
        $var_contain_val = true;
        $this->theJsonResponseShouldHaveAContaining($var_name, $var_contain_val);
        $this->theResponseCodeShouldBe($response_code);
*/
        // If above passed then we move on to mock actions
        $updateIdea = App\Idea::find($this->idea[0]->id);
        PHPUnit::assertNotEquals($updateIdea->user_id, $this->user_id);
}

    /**
     * @Given the idea is private
     */
    public function theIdeaIsPrivate()
    {
        PHPUnit::assertEquals(0, $this->idea[0]->public);
    }
}
