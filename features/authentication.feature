@codeship
Feature:  Authentication
  In order to gain access to the free and premium features of the InnovatorSandbox
  As a visitor or a member
  I want to register and login, and maintain my account information.

  Background:
    Given Fred is a visitor
    And "Barney" has a member account "barney@rubble.com"

  Scenario:  The one where Barney signs in
    When I sign in
    Then I should be logged in

  Scenario:  The one where Fred fails to sign in
    When I sign in with invalid credentials
    Then I should not be logged in

  Scenario:  The one where Fred registers as a member
    When I register "Fred" "fred@flinstone.com"
    Then I should have an account
    And the url should match "activate"

  Scenario:  The one where Fred receives the activation email
    When I register "Fred" "fred@flinstone.com"
    Then "Fred" should receive an activation email

  Scenario:  The one where Fred activates registration
    Given I have an account "Fred" "fred@flinstone.com"
    When I activate account pin "pincode1234"
    Then the url should match "home"
    And I should see "Idea"