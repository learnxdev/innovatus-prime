@wip-api
Feature:  PROJX
  In order to manage my LEARNX ideas
  As a member Barney
  I need to start projects by being able to create, update, and delete LEARNX ideas aka PROJX.

  Background:
    Given I have an account "Barney" "barney@rubble.com"
    And I sign in

  Scenario:  The one where a project is born
    When I add a new project "Number One"
    Then I should see "Next Big Idea"

  Scenario:  The one where a new idea is born
    When I add a new idea "Barney's First Idea"
    Then I should see "Barney's First Idea"

  Scenario:  The one where an idea is private
    Given I own an idea "Barney's First Private Idea"
    And the idea is private
    When I go to "ideas"
    Then I should not see "Barney's First Private Idea"

  Scenario:  The one where an idea is made public
    Given I own an idea "Barney's First Public Idea"
    And I share the idea with everyone
    When I go to "ideas"
    Then I should see "Barney's First Public Idea"

  Scenario:  The one where Betty tries to make Barney's idea public
    Given I own an idea "Barney's Idea"
    And I have an account "Betty" "betty@rubble.com"
    When I sign in
    Then I can not share the idea

  Scenario:  The one where Betty sees Barney's new public idea
    Given I own an idea "Barney's First Public Idea"
    And I share the idea with everyone
    And I have an account "Betty" "betty@rubble.com"
    When I sign in
    And I go to "ideas"
    Then I should see "Barney's First Public Idea"

  Scenario:  The one where Betty can not see Barney's new private idea
    Given I own an idea "Barney's Private Idea"
    And I have an account "Betty" "betty@rubble.com"
    When I sign in
    And I go to "ideas"
    Then I should not see "Barney's Private Idea"